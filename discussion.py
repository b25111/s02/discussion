# [SECTION] Taking input from user
# username = input("Please enter your name:\n")
# print(f"Hello {username}! Welcome to the Python Short Course!")

# # We use the input() option to retrieve input from the user. '\n' creates a new line for the user to type in.


# # if you try to concatenate the value of inputs, it will just combine them since they are both strings. In order for us to get the right numeric result of the addition of 2 numbers, we have to put the two numbers inside the 'int()' function to convert them into numbers first before the addition process.'
# # num1 = input("Enter 1st number:\n")
# # num2 = input("Enter 2nd number:\n")
# # print(f"The sum of num1 and num2 is {int(num1) + int(num2)}")

# # or

# num1 = int(input("First: "))
# num2 = int(input("Second: "))
# print(f"The sum of num1 and num2 is {num1 + num2}")


# [SECTION] If-else Statements
# If-else statements in python don't have curly brackets but instead rely on the spacing of the indention to denote the scope of the statement. It also uses the colon ':' to let the compiler know that the statement continues
test_num = 75

if test_num >= 60:
	print("Test passed")
else:
	print("Test failed")

# test_num2 = int(input("Please input the second test number \n"))

# if test_num2 > 0:
# 	print("The number is positive")
# elif test_num2 == 0:
# 	print("The number is zero")
# else:
#     print("The number is negative")

# [SECTION] Loops
# While loop
# i = 1
# while i <= 5:
#     print(f"Cuurent Count {i}")
#     i+=1

# fruits = ['apple','banana', 'cherry']

# for fruit in fruits:
#     print(fruit)


# range() function


# for x in range(6):
#     print(f"The current value is {x}")

# for x in range(5,10):
#     print(f"The current value is {x}")

# 1st argument: where the range starts
# 2nd argument: where the range ends
# 3rd argument: incrementing of the range
# for x in range(5,20,2):
#     print(f"The current value is {x}")


# [SECTION] Break statement
# j = 1
# while j < 6:
#     print(j)
#     if j == 3:
#         break
#     j += 1

k = 1
while k < 6:
    k += 1
    if k==3:
        continue
    print(k)
